
#ifndef __TRIE__H
#define __TRIE__H

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <stdlib.h>
#include "pair.h"
#include "treap.h"

#define SIZE 8
#define CH (s[0] - '2')
#define NOT_FOUND "Word not found!"

// Returneaza indexul din matrice corespunzator
// literei cerute
int getIndex(char c) {
  if ('a' <= c && c <= 'c') return 0;
  if ('d' <= c && c <= 'f') return 1;
  if ('g' <= c && c <= 'i') return 2;
  if ('j' <= c && c <= 'l') return 3;
  if ('m' <= c && c <= 'o') return 4;
  if ('p' <= c && c <= 's') return 5;
  if ('t' <= c && c <= 'v') return 6;
  if ('w' <= c && c <= 'z') return 7;

  return -1;
}

// Din stringul obtin de la input separa codul corespunzator
// cuvantului cerut si al k-lea termen dupa frecventa(mod)
int getInfo(std::string & s, std::string & code) {
  int mod = 0;

  int p = s.find("*");
  code = s.substr(0, p);

  if (p == -1) {
    mod = 0;
  } else {
    std::string aux = s.substr(p + 1);
    mod = std::strtol(aux.data(), NULL, 10);
  }

  return mod;
}


class Trie {
private:
  Trie * digit[SIZE];
  Treap<Pair> * words;

public:
  Trie() {
    for (int i = 0; i < SIZE; ++i)
      digit[i] = NULL;
    words = NULL;
  }

  ~Trie() {
    delete words;
    for (int i = 0; i < SIZE; ++i) {
      if (digit[i]) {
        delete digit[i];
      }
    }
  }

  void insert(Pair & mypair, std::string s) {
    if (s.length() > 0) {
      int index = getIndex(s[0]);
      if (digit[index] == NULL) {
        digit[index] = new Trie;
      }
      std::string new_s = s.substr(1, s.length() - 1);
        return digit[index]->insert(mypair, new_s);
    } else {
      if (words == NULL)
        words = new Treap<Pair>;
      words->insert(words, mypair, rand() % 5000);
    }
  }

  std::string getWord(std::string & s, int & mod) {
    if (s.length() > 0) {
      // CH reprezinta pozitia in vector a codului cerut
      int index = CH;

      if (digit[index] == NULL)
        return NOT_FOUND;

      std::string new_s = s.substr(1, s.length() - 1);

      return digit[index]->getWord(new_s, mod);
    } else {
        // Pentru al k-lea cuvant cu cea mai mare frecventa(mod)
        // calculam ce pozitia sa porninand de la cea mai mica
        // frecventa(pos), deoarece functia findK cauta al k-lea
        // minim
        int pos = words->nr_nodes - (mod % (words->nr_nodes));

        Pair mypair = words->findK(words, pos);

        // Odata gasit cuvantul il scoatem, ii maresc frecventa
        // si il reintroduc in treap
        words->erase(words, mypair);
        mypair.update(); // creste frecventa cu o unitate
        words->insert(words, mypair, rand() % 5000);

        // returneaza cuvantul cerut
        return mypair.word;
    }
  }
};

#endif
