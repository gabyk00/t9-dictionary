
#ifndef __PAIR__H
#define __PAIR__H

// Structura care va fi retinuta in treapuri, cuvantul si frecventa acestuia
struct Pair {
  std::string word;
  int frequency;

  // creste frecventa cuvantului
  void update() {
    ++frequency;
  }

  bool operator< (Pair & other) {
    if (frequency == other.frequency)
      return word > other.word;
    else
      return frequency < other.frequency;
  }

  bool operator> (Pair & other) {
    if (frequency == other.frequency)
      return word < other.word;
    else
      return frequency > other.frequency;
  }

  bool operator== (Pair & other) {
    return frequency == other.frequency &&
         word      == other.word;
  }
};

#endif
