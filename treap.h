
#ifndef __TREAP__H
#define __TREAP__H

#include <iostream>
#include <fstream>
#include <cstdlib>


template <typename T> struct Treap {
  T key;
  int priority;
  Treap<T> *left, *right;
  bool nil;
  int nr_nodes;

  Treap() : priority(-1), left(NULL), right(NULL), nil(true), nr_nodes(0) {}

  ~Treap() {
    if (left)
      delete left;
    if (right)
      delete right;
  }

  void addData(Treap<T> * fatherPointer, T key, int priority) {
    fatherPointer->nil = false;
    fatherPointer->key = key;
    fatherPointer->priority = priority;
    fatherPointer->nr_nodes = 1;
    fatherPointer->left = new Treap();
    fatherPointer->right = new Treap();
  }

  void delData(Treap<T> * fatherPointer) {
    fatherPointer->nil = true;
    fatherPointer->priority = -1;
    delete fatherPointer->left;
    delete fatherPointer->right;
    fatherPointer->left = fatherPointer->right = NULL;
    fatherPointer->nr_nodes = 0;
  }

  bool isNil(Treap<T> * fatherPointer) {
    return fatherPointer->nil;
  }

  bool find(Treap<T> * fatherPointer, T key) {
    if ( isNil(fatherPointer) ) {
      return false;
    }

    if ( key == fatherPointer->key )
      return true;
    else if ( key < fatherPointer->key )
      return find(fatherPointer->left, key);
    else
      return find(fatherPointer->right, key);
  }

  Treap<T> * rotateRight(Treap<T> *&nod) {
    int count_nodes1 = nod->nr_nodes;
    int count_nodes2 = nod->right->nr_nodes + 1;

    if ( !isNil(nod->left) )
      count_nodes2 += nod->left->right->nr_nodes;

    Treap<T> * aux = nod->left;
    nod->left = aux->right;
    aux->right = nod;

    aux->nr_nodes = count_nodes1;
    aux->right->nr_nodes = count_nodes2;

    return aux;
  }

  Treap<T> * rotateLeft(Treap<T> *&nod) {
    int count_nodes1 = nod->nr_nodes;
    int count_nodes2 = nod->left->nr_nodes + 1;

    if ( !isNil(nod->right) )
      count_nodes2 += nod->right->left->nr_nodes;

    Treap<T> * aux = nod->right;
    nod->right = aux->left;
    aux->left = nod;

    aux->nr_nodes = count_nodes1;
    aux->left->nr_nodes = count_nodes2;

    return aux;
  }

  void insert(Treap<T> *&fatherPointer, T key, int priority) {
    if ( isNil(fatherPointer) ) {
      addData(fatherPointer, key, priority);

      return ;
    }

    if (key < fatherPointer->key) {
      ++fatherPointer->nr_nodes;
      insert(fatherPointer->left, key, priority);
    } else {
      ++fatherPointer->nr_nodes;
      insert(fatherPointer->right, key, priority);
    }

    if (fatherPointer->left->priority > fatherPointer->priority) {
      fatherPointer = rotateRight(fatherPointer);
    } else if (fatherPointer->right->priority > fatherPointer->priority) {
      fatherPointer = rotateLeft(fatherPointer);
    }
  }

  void erase(Treap<T> *&fatherPointer, T key) {
    if ( isNil(fatherPointer) ) {
      return ;
    }

    if (key < fatherPointer->key) {
      --fatherPointer->nr_nodes;
      erase(fatherPointer->left, key);
    } else if (key > fatherPointer->key) {
      --fatherPointer->nr_nodes;
      erase(fatherPointer->right, key);
    } else {
      if ( isNil(fatherPointer->left) && isNil(fatherPointer->right) )
        return delData(fatherPointer);
      else if (fatherPointer->left->priority > fatherPointer->right->priority)
        fatherPointer = rotateRight(fatherPointer);
      else if (fatherPointer->right->priority >= fatherPointer->left->priority)
        fatherPointer = rotateLeft(fatherPointer);

      erase(fatherPointer, key);
    }
  }

  T findK(Treap<T> * fatherPointer, int k) {
    if ( isNil(fatherPointer) ) {
      T x;
      std::cout << "Error! " << k << "-th element not available!\n";
      return x;
    }

    // pozitia nodului curent
    int pos = fatherPointer->nr_nodes - fatherPointer->right->nr_nodes;

    if (pos == k)
      return fatherPointer->key;
    else if (k < pos)
      return findK(fatherPointer->left, k);
    else
      return findK(fatherPointer->right, k - pos);
  }
};

#endif
