#include <iostream>
#include <string>
#include <fstream>

#include "treap.h"
#include "trie.h"
#include "pair.h"


int main() {
  std::ifstream fin;
  std::ofstream fout;
  Trie * mytrie = new Trie;
  std::string buffer, code;
  Pair mypair;
  int mod;

  fin.open("date.in");
  fout.open("date.out");

  int n;
  fin >> n;

  for (int i = 0; i < n; ++i) {
    fin >> mypair.word;
    fin >> mypair.frequency;

    mytrie->insert(mypair, mypair.word);
  }

  fin >> n;

  for (int i = 0; i < n; ++i) {
    fin >> buffer;
    // mod - al k-lea cuvant cerut
    // cod - codul cuvantului cerut
    mod = getInfo(buffer, code);

    buffer = mytrie->getWord(code, mod);
    fout << buffer << '\n';
  }

  delete mytrie;

  fin.close();
  fout.close();

  return 0;
}
